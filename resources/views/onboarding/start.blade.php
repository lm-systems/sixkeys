<x-app-layout>
  <x-slot name="module">
    {{ __('secure-enclave') }}
  </x-slot>
    <div class="grid grid-cols-2 md:grid-cols-2 grid-rows-2 md:grid-rows-2 w-11/12 mx-auto">

      <main class="flex-1 relative z-0 overflow-y-auto focus:outline-none">
          <div class="py-6">
            <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                {{-- <x-slot name="header">
                    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                        {{ __('Start') }}
                    </h2>
                </x-slot> --}}
            </div>
            <div class="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
                <x-slot name="content">
                    <div class="bg-white overflow-hidden shadow rounded-lg divide-y divide-gray-200 mt-5">
                      <div class="px-4 py-5 sm:px-6">
                          <div class="-ml-4 -mt-2 flex items-center justify-between flex-wrap sm:flex-nowrap">
                            <div class="ml-4 mt-2">
                              <h3 class="text-lg leading-6 font-medium text-gray-900">
                                Getting Started
                              </h3>
                            </div>
                            <div class="ml-4 mt-2 flex-shrink-0">
                              {{-- <button type="button" class="relative inline-flex items-center px-4 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                Add new contact
                              </button> --}}
                            </div>
                          </div>
                      </div>
                      <div class="">
                        <div class="flex flex-col">
                          <div class="-my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
                            <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
                              <div class="shadow overflow-hidden border-b border-gray-200 sm:rounded-lg">
                                <div class="p-5 text-md">
                                The getting started wizard will help you fill in the information that is most important to you. Check the boxes below for information categories that are a priority and we can get started recording your important information.
                                </div>
                                <div class="p-5">
                                  <form class="space-y-6" action="#" method="POST">
                                    <div class="grid grid-cols-3 gap-4">
                                      <fieldset>
                                        <div class="mt-4 space-y-4">
                                          <div class="flex items-start">
                                            <div class="h-5 flex items-center">
                                              <input id="comments" name="comments" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded" checked>
                                            </div>
                                            <div class="ml-3 text-sm">
                                              <label for="comments" class="font-medium text-gray-700">Personal Information</label>
                                              <p class="text-gray-500">This is a label that provides more explanation.</p>
                                            </div>
                                          </div>
                                          <div class="flex items-start">
                                            <div class="flex items-center h-5">
                                              <input id="candidates" name="candidates" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded" checked>
                                            </div>
                                            <div class="ml-3 text-sm">
                                              <label for="candidates" class="font-medium text-gray-700">Beneficiaries</label>
                                              <p class="text-gray-500">This is a label that provides more explanation.</p>
                                            </div>
                                          </div>
                                          <div class="flex items-start">
                                            <div class="flex items-center h-5">
                                              <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded" checked>
                                            </div>
                                            <div class="ml-3 text-sm">
                                              <label for="offers" class="font-medium text-gray-700">Finances</label>
                                              <p class="text-gray-500">This is a label that provides more explanation.</p>
                                            </div>
                                          </div>
                                          <div class="flex items-start">
                                            <div class="flex items-center h-5">
                                              <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded" >
                                            </div>
                                            <div class="ml-3 text-sm">
                                              <label for="offers" class="font-medium text-gray-700">Financial Advisors</label>
                                              <p class="text-gray-500">This is a label that provides more explanation.</p>
                                            </div>
                                          </div>
                                          <div class="flex items-start">
                                            <div class="h-5 flex items-center">
                                              <input id="comments" name="comments" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded" checked>
                                            </div>
                                            <div class="ml-3 text-sm">
                                              <label for="comments" class="font-medium text-gray-700">Will and Executors</label>
                                              <p class="text-gray-500">This is a label that provides more explanation.</p>
                                            </div>
                                          </div>
                                        </div>
                                      </fieldset>
                                      <fieldset>
                                        <div class="mt-4 space-y-4">
                                          <div class="flex items-start">
                                            <div class="flex items-center h-5">
                                              <input id="candidates" name="candidates" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded" checked>
                                            </div>
                                            <div class="ml-3 text-sm">
                                              <label for="candidates" class="font-medium text-gray-700">Physical Assets</label>
                                              <p class="text-gray-500">This is a label that provides more explanation.</p>
                                            </div>
                                          </div>
                                          <div class="flex items-start">
                                            <div class="flex items-center h-5">
                                              <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                            </div>
                                            <div class="ml-3 text-sm">
                                              <label for="offers" class="font-medium text-gray-700">Business Information</label>
                                              <p class="text-gray-500">This is a label that provides more explanation.</p>
                                            </div>
                                          </div>
                                          <div class="flex items-start">
                                            <div class="flex items-center h-5">
                                              <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                            </div>
                                            <div class="ml-3 text-sm">
                                              <label for="offers" class="font-medium text-gray-700">Lifecycle Documents</label>
                                              <p class="text-gray-500">This is a label that provides more explanation.</p>
                                            </div>
                                          </div>
                                          <div class="flex items-start">
                                            <div class="h-5 flex items-center">
                                              <input id="comments" name="comments" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                            </div>
                                            <div class="ml-3 text-sm">
                                              <label for="comments" class="font-medium text-gray-700">Digital World</label>
                                              <p class="text-gray-500">This is a label that provides more explanation.</p>
                                            </div>
                                          </div>
                                          <div class="flex items-start">
                                            <div class="flex items-center h-5">
                                              <input id="candidates" name="candidates" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                            </div>
                                            <div class="ml-3 text-sm">
                                              <label for="candidates" class="font-medium text-gray-700">Subscriptions/Memberships</label>
                                              <p class="text-gray-500">This is a label that provides more explanation.</p>
                                            </div>
                                          </div>
                                        </div>
                                      </fieldset>
                                      <fieldset>
                                        <legend class="text-base font-medium text-gray-900"> </legend>
                                        <div class="mt-4 space-y-4">
                                          
                                          <div class="flex items-start">
                                            <div class="flex items-center h-5">
                                              <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                            </div>
                                            <div class="ml-3 text-sm">
                                              <label for="offers" class="font-medium text-gray-700">Health Information</label>
                                              <p class="text-gray-500">This is a label that provides more explanation.</p>
                                            </div>
                                          </div>
                                          <div class="flex items-start">
                                            <div class="flex items-center h-5">
                                              <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                            </div>
                                            <div class="ml-3 text-sm">
                                              <label for="offers" class="font-medium text-gray-700">Requests for when I die</label>
                                              <p class="text-gray-500">This is a label that provides more explanation.</p>
                                            </div>
                                          </div>
                                          <div class="flex items-start">
                                            <div class="flex items-center h-5">
                                              <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                            </div>
                                            <div class="ml-3 text-sm">
                                              <label for="offers" class="font-medium text-gray-700">House</label>
                                              <p class="text-gray-500">This is a label that provides more explanation.</p>
                                            </div>
                                          </div>
                                          <div class="flex items-start">
                                            <div class="flex items-center h-5">
                                              <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                            </div>
                                            <div class="ml-3 text-sm">
                                              <label for="offers" class="font-medium text-gray-700">Information for loved ones</label>
                                              <p class="text-gray-500">This is a label that provides more explanation.</p>
                                            </div>
                                          </div>
                                        </div>
                                      </fieldset>
                                    </div>
                                    <div class="pt-5">
                                      <div class="flex justify-end">
                                        <a href="/onboarding/personal" type="button" class="inline-flex items-center px-4 py-2 border border-transparent text-base font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                          Get Started
                                        </a>
                                      </div>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>

                      </div>
                    </div>
                </x-slot>
            </div>
          </div>
        </main>
    </div>
</x-app-layout>