<x-app-layout>
  <x-slot name="module">
    {{ __('secure-enclave') }}
  </x-slot>
    <div class="grid grid-cols-2 md:grid-cols-2 grid-rows-2 md:grid-rows-2 w-11/12 mx-auto">

      <main class="flex-1 relative z-0 overflow-y-auto focus:outline-none">
          <div class="py-6">
            <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <x-slot name="header">
                    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                        {{ __('Getting Started') }}
                    </h2>
                </x-slot>
            </div>
            <div class="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
                <x-slot name="content">
                    <div class="lg:grid lg:grid-cols-12 lg:gap-x-5">
                    <aside class="py-6 px-2 sm:px-6 lg:py-0 lg:px-0 lg:col-span-3">
                      <nav aria-label="Progress">
                        <ol class="overflow-hidden">
                          <li class="relative pb-10">
                            <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-indigo-600" aria-hidden="true"></div>
                            <!-- Complete Step -->
                            <a href="/onboarding/personal" class="relative flex items-start group" aria-current="step">
                              <span class="h-9 flex items-center" aria-hidden="true">
                                <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-indigo-600 rounded-full">
                                  <span class="h-2.5 w-2.5 bg-indigo-600 rounded-full"></span>
                                </span>
                              </span>
                              <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-indigo-600">Personal Information</span>
                                <span class="text-sm text-gray-500">Cursus semper viverra facilisis et et some more.</span>
                              </span>
                            </a>
                          </li>

                          <li class="relative pb-10">
                            <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
                            <!-- Current Step -->
                            <a href="/onboarding/beneficiaries" class="relative flex items-start group">
                              <span class="h-9 flex items-center" aria-hidden="true">
                                <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                                  <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                                </span>
                              </span>
                              <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Beneficiaries</span>
                                <span class="text-sm text-gray-500">Penatibus eu quis ante.</span>
                              </span>
                            </a>
                          </li>

                          <li class="relative pb-10">
                            <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
                            <!-- Upcoming Step -->
                            <a href="/onboarding/finances" class="relative flex items-start group">
                              <span class="h-9 flex items-center" aria-hidden="true">
                                <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                                  <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                                </span>
                              </span>
                              <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Finances</span>
                                <span class="text-sm text-gray-500">Penatibus eu quis ante.</span>
                              </span>
                            </a>
                          </li>

                          <li class="relative pb-10">
                            <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
                            <!-- Upcoming Step -->
                            <a href="/onboarding/will" class="relative flex items-start group">
                              <span class="h-9 flex items-center" aria-hidden="true">
                                <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                                  <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                                </span>
                              </span>
                              <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Will and Executor</span>
                                <span class="text-sm text-gray-500">Faucibus nec enim leo et.</span>
                              </span>
                            </a>
                          </li>

                          <li class="relative pb-10">
                            <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
                            <!-- Upcoming Step -->
                            <a href="/onboarding/physical" class="relative flex items-start group">
                              <span class="h-9 flex items-center" aria-hidden="true">
                                <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                                  <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                                </span>
                              </span>
                              <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Physical Assets</span>
                                <span class="text-sm text-gray-500">Faucibus nec enim leo et.</span>
                              </span>
                            </a>
                          </li>

                          <li class="relative pb-10">
                            <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
                            <!-- Upcoming Step -->
                            <a href="/onboarding/guardians" class="relative flex items-start group">
                              <span class="h-9 flex items-center" aria-hidden="true">
                                <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                                  <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                                </span>
                              </span>
                              <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Guardians</span>
                                <span class="text-sm text-gray-500">Assign up to 5 Guardians to protect your vault.</span>
                              </span>
                            </a>
                          </li>

                          <li class="relative">
                            <!-- Upcoming Step -->
                            <a href="/onboarding/finish" class="relative flex items-start group">
                              <span class="h-9 flex items-center" aria-hidden="true">
                                <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                                  <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                                </span>
                              </span>
                              <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Finish</span>
                                <span class="text-sm text-gray-500">Review your information and finish the getting started wizard.</span>
                              </span>
                            </a>
                          </li>
                          
                        </ol>
                      </nav>
                    </aside>

                    

                    <div class="space-y-6 sm:px-6 lg:px-0 lg:col-span-9">
                      <form action="#" method="POST">
                        <div class="shadow sm:rounded-md sm:overflow-hidden">
                          <div class="bg-white py-6 px-4 space-y-6 sm:p-6">
                            <div>
                              <h3 class="text-lg leading-6 font-medium text-gray-900">Personal Information</h3>
                              <p class="mt-1 text-sm text-gray-500">This area provides a space to store your personal information. Remember all information stored in Six Keys is private and secure.</p>
                            </div>

                            <div class="grid grid-cols-6 gap-6">
                              <div class="col-span-6 sm:col-span-3">
                                <label for="first_name" class="block text-sm font-medium text-gray-700">First name</label>
                                <input type="text" name="first_name" id="first_name" autocomplete="given-name" class="mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                              </div>

                              <div class="col-span-6 sm:col-span-3">
                                <label for="last_name" class="block text-sm font-medium text-gray-700">Last name</label>
                                <input type="text" name="last_name" id="last_name" autocomplete="family-name" class="mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                              </div>
                              <div class="col-span-6 sm:col-span-3">
                                <label for="last_name" class="block text-sm font-medium text-gray-700">Middle name</label>
                                <input type="text" name="last_name" id="last_name" autocomplete="family-name" class="mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                              </div>
                              <div class="col-span-6 sm:col-span-3">
                                <label for="last_name" class="block text-sm font-medium text-gray-700">Other names</label>
                                <input type="text" name="last_name" id="last_name" autocomplete="family-name" class="mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                              </div>

                              <div class="col-span-6 sm:col-span-4">
                                <label for="email_address" class="block text-sm font-medium text-gray-700">Email address</label>
                                <input type="text" name="email_address" id="email_address" autocomplete="email" class="mt-1 block w-full border border-gray-300 rounded-md shadow-sm py-2 px-3 focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                              </div>

                              
                            </div>
                          </div>
                          <div class="px-4 py-3 bg-gray-50 text-right sm:px-6">
                            <button type="submit" class="bg-indigo-600 border border-transparent rounded-md shadow-sm py-2 px-4 inline-flex justify-center text-sm font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                              Save
                            </button>
                          </div>
                        </div>
                      </form>
                      <div class="flex justify-end px-5">
                        <a href="/onboarding/beneficiaries" type="button" class="inline-flex items-center px-4 py-2 border border-transparent text-base font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                          Next
                        </a>
                      </div>
                    </div>
                  </div>
                </x-slot>
            </div>
          </div>
        </main>
    </div>
</x-app-layout>