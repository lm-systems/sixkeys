<x-app-layout>
  <x-slot name="module">
    {{ __('secure-enclave') }}
  </x-slot>
    <div class="grid grid-cols-2 md:grid-cols-2 grid-rows-2 md:grid-rows-2 w-11/12 mx-auto">

      <main class="flex-1 relative z-0 overflow-y-auto focus:outline-none">
          <div class="py-6">
            <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <x-slot name="header">
                    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                        {{ __('Getting Started') }}
                    </h2>
                </x-slot>
            </div>
            <div class="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
                <x-slot name="content">
                    <div class="lg:grid lg:grid-cols-12 lg:gap-x-5">
                    <aside class="py-6 px-2 sm:px-6 lg:py-0 lg:px-0 lg:col-span-3">
                      <nav aria-label="Progress">
                        <ol class="overflow-hidden">
                          <li class="relative pb-10">
                            <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-indigo-600" aria-hidden="true"></div>
                            <!-- Complete Step -->
                            <a href="/onboarding/personal" class="relative flex items-start group">
                            <span class="h-9 flex items-center">
                              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-indigo-600 rounded-full group-hover:bg-indigo-800">
                                <!-- Heroicon name: solid/check -->
                                <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg>
                              </span>
                            </span>
                            <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-indigo-600">Personal Information</span>
                                <span class="text-sm text-gray-500">Cursus semper viverra facilisis et et some more.</span>
                              </span>
                            </a>
                          </li>

                          <li class="relative pb-10">
                            <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-indigo-600" aria-hidden="true"></div>
                            <!-- Complete Step -->
                            <a href="/onboarding/beneficiaries" class="relative flex items-start group">
                            <span class="h-9 flex items-center">
                              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-indigo-600 rounded-full group-hover:bg-indigo-800">
                                <!-- Heroicon name: solid/check -->
                                <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg>
                              </span>
                            </span>
                              <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Beneficiaries</span>
                                <span class="text-sm text-gray-500">Penatibus eu quis ante.</span>
                              </span>
                            </a>
                          </li>

                          <li class="relative pb-10">
                            <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-indigo-600" aria-hidden="true"></div>
                            <!-- Complete Step -->
                            <a href="/onboarding/finances" class="relative flex items-start group">
                            <span class="h-9 flex items-center">
                              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-indigo-600 rounded-full group-hover:bg-indigo-800">
                                <!-- Heroicon name: solid/check -->
                                <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg>
                              </span>
                            </span>
                              <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Finances</span>
                                <span class="text-sm text-gray-500">Penatibus eu quis ante.</span>
                              </span>
                            </a>
                          </li>

                          <li class="relative pb-10">
                            <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-indigo-600" aria-hidden="true"></div>
                            <!-- Complete Step -->
                            <a href="/onboarding/will" class="relative flex items-start group">
                            <span class="h-9 flex items-center">
                              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-indigo-600 rounded-full group-hover:bg-indigo-800">
                                <!-- Heroicon name: solid/check -->
                                <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg>
                              </span>
                            </span>
                              <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Will and Executor</span>
                                <span class="text-sm text-gray-500">Faucibus nec enim leo et.</span>
                              </span>
                            </a>
                          </li>

                          <li class="relative pb-10">
                            <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-indigo-600" aria-hidden="true"></div>
                            <!-- Complete Step -->
                            <a href="/onboarding/physical" class="relative flex items-start group">
                            <span class="h-9 flex items-center">
                              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-indigo-600 rounded-full group-hover:bg-indigo-800">
                                <!-- Heroicon name: solid/check -->
                                <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg>
                              </span>
                            </span>
                              <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Physical Assets</span>
                                <span class="text-sm text-gray-500">Faucibus nec enim leo et.</span>
                              </span>
                            </a>
                          </li>

                          <li class="relative pb-10">
                            <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-indigo-600" aria-hidden="true"></div>
                            <!-- Complete Step -->
                            <a href="/onboarding/guardians" class="relative flex items-start group">
                            <span class="h-9 flex items-center">
                              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-indigo-600 rounded-full group-hover:bg-indigo-800">
                                <!-- Heroicon name: solid/check -->
                                <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg>
                              </span>
                            </span>
                              <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Guardians</span>
                                <span class="text-sm text-gray-500">Assign up to 5 Guardians to protect your vault.</span>
                              </span>
                            </a>
                          </li>

                          <li class="relative">
                            <!-- Upcoming Step -->
                            <a href="/onboarding/finish" class="relative flex items-start group" aria-current="step">
                              <span class="h-9 flex items-center" aria-hidden="true">
                                <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-indigo-600 rounded-full">
                                  <span class="h-2.5 w-2.5 bg-indigo-600 rounded-full"></span>
                                </span>
                              </span>
                              <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Finish</span>
                                <span class="text-sm text-gray-500">Review your information and finish the getting started wizard.</span>
                              </span>
                            </a>
                          </li>
                          
                        </ol>
                      </nav>
                    </aside>
                    <div class="space-y-6 sm:px-6 lg:px-0 lg:col-span-9">
                      <div class="bg-white overflow-hidden shadow rounded-lg divide-y divide-gray-200">
                        <div class="px-4 py-5 sm:px-6">
                          <div class="bg-white px-4 py-5 sm:px-6">
                            <div class="-ml-4 -mt-4 flex justify-between items-center flex-wrap sm:flex-nowrap">
                              <div class="ml-4 mt-4">
                                <h3 class="text-lg leading-6 font-medium text-gray-900">
                                  Finish
                                </h3>
                                <p class="mt-1 text-sm text-gray-500">
                                  Lorem ipsum dolor sit amet consectetur adipisicing elit quam corrupti consectetur.
                                </p>
                              </div>
                              <div class="ml-4 mt-4 flex-shrink-0">
                                {{-- <button type="button" class="relative inline-flex items-center px-4 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                  Add new beneficiary
                                </button> --}}
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="px-4 py-5 sm:p-6">
                          <div class="px-4 py-5 sm:p-6">
                            <p>Congratulations on finishing the getting started wizard! You are now on your way to ensuring that if anything were to happen to you, your legacy would be safeguarded.</p>

                            <p class="mt-5 text-lg">Completed categories:</p>

                            <a href="" class="group flex items-center px-2 py-2 text-base font-medium">
                              <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mx-2" viewBox="0 0 20 20" fill="green">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                                  </svg>
                              Personal Information
                            </a>
                            <a href="" class="group flex items-center px-2 py-2 text-base font-medium">
                              <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mx-2" viewBox="0 0 20 20" fill="green">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                                  </svg>
                              Beneficiaries
                            </a>
                            <a href="" class="group flex items-center px-2 py-2 text-base font-medium">
                              <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mx-2" viewBox="0 0 20 20" fill="green">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                                  </svg>
                              Finances
                            </a>
                            <a href="" class="group flex items-center px-2 py-2 text-base font-medium">
                              <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mx-2" viewBox="0 0 20 20" fill="green">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                                  </svg>
                              Will and Executors
                            </a>
                            <a href="" class="group flex items-center px-2 py-2 text-base font-medium">
                              <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mx-2" viewBox="0 0 20 20" fill="green">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                                  </svg>
                              Physical Assets
                            </a>
                            <a href="" class="group flex items-center px-2 py-2 text-base font-medium">
                              <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5 mx-2" viewBox="0 0 20 20" fill="green">
                                    <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                                  </svg>
                              Assigned guardians
                            </a>
                          </div>
                          
                        </div>

                      </div>
                      <div class="flex justify-end px-5 gap-4">
                            <a href="/onboarding/guardians" type="button" class="inline-flex items-center px-4 py-2 border border-transparent text-base font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                              Back
                            </a>
                            <a href="/dashboard" type="button" class="inline-flex items-center px-4 py-2 border border-transparent text-base font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                              Dashboard
                            </a>
                          </div>
                    </div>

                  </div>
                </x-slot>
            </div>
          </div>
        </main>
    </div>
</x-app-layout>