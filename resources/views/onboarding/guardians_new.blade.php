<x-app-layout>
  <x-slot name="module">
    {{ __('secure-enclave') }}
  </x-slot>
    <div class="grid grid-cols-2 md:grid-cols-2 grid-rows-2 md:grid-rows-2 w-11/12 mx-auto">

      <main class="flex-1 relative z-0 overflow-y-auto focus:outline-none">
          <div class="py-6">
            <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <x-slot name="header">
                    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                        {{ __('New Guardian') }}
                    </h2>
                </x-slot>
            </div>
            <div class="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
                <x-slot name="content">
                    <div class="space-y-6">
                          

                      <div class="bg-white shadow px-4 py-5 sm:rounded-lg sm:p-6">
                        <div class="md:grid md:grid-cols-3 md:gap-6">
                          <div class="md:col-span-1">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">Guardian Information</h3>
                            <p class="mt-1 text-sm text-gray-500">
                              Use a permanent address where the guardian can receive mail.
                            </p>
                          </div>
                          <div class="mt-5 md:mt-0 md:col-span-2">
                            <form action="#" method="POST">
                              <div class="grid grid-cols-6 gap-6">
                                <div class="col-span-6 sm:col-span-3">
                                  <label for="first_name" class="block text-sm font-medium text-gray-700">First name</label>
                                  <input type="text" name="first_name" id="first_name" autocomplete="given-name" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                </div>

                                <div class="col-span-6 sm:col-span-3">
                                  <label for="last_name" class="block text-sm font-medium text-gray-700">Last name</label>
                                  <input type="text" name="last_name" id="last_name" autocomplete="family-name" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                </div>

                                <div class="col-span-6 sm:col-span-4">
                                  <label for="email_address" class="block text-sm font-medium text-gray-700">Email address</label>
                                  <input type="text" name="email_address" id="email_address" autocomplete="email" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                </div>

                                <div class="col-span-6 sm:col-span-3">
                                  <label for="country" class="block text-sm font-medium text-gray-700">Country / Region</label>
                                  <select id="country" name="country" autocomplete="country" class="mt-1 block w-full py-2 px-3 border border-gray-300 bg-white rounded-md shadow-sm focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 sm:text-sm">
                                    <option>United States</option>
                                    <option>Canada</option>
                                    <option>Mexico</option>
                                  </select>
                                </div>

                                <div class="col-span-6">
                                  <label for="street_address" class="block text-sm font-medium text-gray-700">Street address</label>
                                  <input type="text" name="street_address" id="street_address" autocomplete="street-address" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                </div>

                                <div class="col-span-6 sm:col-span-6 lg:col-span-2">
                                  <label for="city" class="block text-sm font-medium text-gray-700">City</label>
                                  <input type="text" name="city" id="city" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                </div>

                                <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                                  <label for="state" class="block text-sm font-medium text-gray-700">State / Province</label>
                                  <input type="text" name="state" id="state" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                </div>

                                <div class="col-span-6 sm:col-span-3 lg:col-span-2">
                                  <label for="postal_code" class="block text-sm font-medium text-gray-700">ZIP / Postal</label>
                                  <input type="text" name="postal_code" id="postal_code" autocomplete="postal-code" class="mt-1 focus:ring-indigo-500 focus:border-indigo-500 block w-full shadow-sm sm:text-sm border-gray-300 rounded-md">
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>

                      <div class="bg-white shadow px-4 py-5 sm:rounded-lg sm:p-6">
                        <div class="md:grid md:grid-cols-3 md:gap-6">
                          <div class="md:col-span-1">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">Access Level</h3>
                            <p class="mt-1 text-sm text-gray-500">
                              Decide what level of access your guardian should have in the event your vault needs to be accessed.
                            </p>
                          </div>
                          <div class="mt-5 md:mt-0 md:col-span-2">
                            <form class="space-y-6" action="#" method="POST">
                              <fieldset>
                                {{-- <legend class="text-base font-medium text-gray-900">By Email</legend> --}}
                                <div class="mt-4 space-y-4">
                                  <div class="flex items-start">
                                    <div class="h-5 flex items-center">
                                      <input id="comments" name="comments" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                    </div>
                                    <div class="ml-3 text-sm">
                                      <label for="comments" class="font-medium text-gray-700">Personal Information</label>
                                      {{-- <p class="text-gray-500">Get notified when someones posts a comment on a posting.</p> --}}
                                    </div>
                                  </div>
                                  <div class="flex items-start">
                                    <div class="flex items-center h-5">
                                      <input id="candidates" name="candidates" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                    </div>
                                    <div class="ml-3 text-sm">
                                      <label for="candidates" class="font-medium text-gray-700">Beneficiaries</label>
                                      {{-- <p class="text-gray-500">Get notified when a candidate applies for a job.</p> --}}
                                    </div>
                                  </div>
                                  <div class="flex items-start">
                                    <div class="flex items-center h-5">
                                      <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                    </div>
                                    <div class="ml-3 text-sm">
                                      <label for="offers" class="font-medium text-gray-700">Finances</label>
                                      {{-- <p class="text-gray-500">Get notified when a candidate accepts or rejects an offer.</p> --}}
                                    </div>
                                  </div>
                                  <div class="flex items-start">
                                    <div class="flex items-center h-5">
                                      <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                    </div>
                                    <div class="ml-3 text-sm">
                                      <label for="offers" class="font-medium text-gray-700">Financial Advisors</label>
                                      {{-- <p class="text-gray-500">Get notified when a candidate accepts or rejects an offer.</p> --}}
                                    </div>
                                  </div>
                                  <div class="flex items-start">
                                    <div class="flex items-center h-5">
                                      <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                    </div>
                                    <div class="ml-3 text-sm">
                                      <label for="offers" class="font-medium text-gray-700">Will and Executors</label>
                                      {{-- <p class="text-gray-500">Get notified when a candidate accepts or rejects an offer.</p> --}}
                                    </div>
                                  </div>
                                  <div class="flex items-start">
                                    <div class="flex items-center h-5">
                                      <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                    </div>
                                    <div class="ml-3 text-sm">
                                      <label for="offers" class="font-medium text-gray-700">Physical Assets</label>
                                      {{-- <p class="text-gray-500">Get notified when a candidate accepts or rejects an offer.</p> --}}
                                    </div>
                                  </div>
                                  <div class="flex items-start">
                                    <div class="flex items-center h-5">
                                      <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                    </div>
                                    <div class="ml-3 text-sm">
                                      <label for="offers" class="font-medium text-gray-700">Business Information</label>
                                      {{-- <p class="text-gray-500">Get notified when a candidate accepts or rejects an offer.</p> --}}
                                    </div>
                                  </div>
                                  <div class="flex items-start">
                                    <div class="flex items-center h-5">
                                      <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                    </div>
                                    <div class="ml-3 text-sm">
                                      <label for="offers" class="font-medium text-gray-700">Lifecycle Documents</label>
                                      {{-- <p class="text-gray-500">Get notified when a candidate accepts or rejects an offer.</p> --}}
                                    </div>
                                  </div>
                                  <div class="flex items-start">
                                    <div class="flex items-center h-5">
                                      <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                    </div>
                                    <div class="ml-3 text-sm">
                                      <label for="offers" class="font-medium text-gray-700">Digital World</label>
                                      {{-- <p class="text-gray-500">Get notified when a candidate accepts or rejects an offer.</p> --}}
                                    </div>
                                  </div>
                                  <div class="flex items-start">
                                    <div class="flex items-center h-5">
                                      <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                    </div>
                                    <div class="ml-3 text-sm">
                                      <label for="offers" class="font-medium text-gray-700">Subscriptions/Memberships</label>
                                      {{-- <p class="text-gray-500">Get notified when a candidate accepts or rejects an offer.</p> --}}
                                    </div>
                                  </div>
                                  <div class="flex items-start">
                                    <div class="flex items-center h-5">
                                      <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                    </div>
                                    <div class="ml-3 text-sm">
                                      <label for="offers" class="font-medium text-gray-700">Health Information</label>
                                      {{-- <p class="text-gray-500">Get notified when a candidate accepts or rejects an offer.</p> --}}
                                    </div>
                                  </div>
                                  <div class="flex items-start">
                                    <div class="flex items-center h-5">
                                      <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                    </div>
                                    <div class="ml-3 text-sm">
                                      <label for="offers" class="font-medium text-gray-700">Requests for when I die</label>
                                      {{-- <p class="text-gray-500">Get notified when a candidate accepts or rejects an offer.</p> --}}
                                    </div>
                                  </div>
                                  <div class="flex items-start">
                                    <div class="flex items-center h-5">
                                      <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                    </div>
                                    <div class="ml-3 text-sm">
                                      <label for="offers" class="font-medium text-gray-700">House</label>
                                      {{-- <p class="text-gray-500">Get notified when a candidate accepts or rejects an offer.</p> --}}
                                    </div>
                                  </div>
                                  <div class="flex items-start">
                                    <div class="flex items-center h-5">
                                      <input id="offers" name="offers" type="checkbox" class="focus:ring-indigo-500 h-4 w-4 text-indigo-600 border-gray-300 rounded">
                                    </div>
                                    <div class="ml-3 text-sm">
                                      <label for="offers" class="font-medium text-gray-700">Information for loved ones</label>
                                      {{-- <p class="text-gray-500">Get notified when a candidate accepts or rejects an offer.</p> --}}
                                    </div>
                                  </div>
                                </div>
                              </fieldset>
                            </form>
                          </div>
                        </div>
                      </div>

                      <div class="bg-white shadow px-4 py-5 sm:rounded-lg sm:p-6">
                        <div class="md:grid md:grid-cols-3 md:gap-6">
                          <div class="md:col-span-1">
                            <h3 class="text-lg font-medium leading-6 text-gray-900">Final Steps</h3>
                            {{-- <p class="mt-1 text-sm text-gray-500">
                              Decide what level of access your guardian should have in the event your vault needs to be accessed.
                            </p> --}}
                          </div>
                          <div class="mt-5 md:mt-0 md:col-span-2">
                            <p class="mt-1 text-sm text-gray-500">Once your guardian has accepted the role as guardian of your vault we will send:</p>
                            <ol>
                              <li class="mt-1 text-sm text-gray-500">1 x Guardian Key</li>
                              <li class="mt-1 text-sm text-gray-500">1 x Protective Sleeve</li>
                              <li class="mt-1 text-sm text-gray-500">1 x Getting started guide</li>
                            </ol>
                          </div>
                        </div>
                      </div>
                      <div class="flex justify-end">
                        <button type="button" class="bg-white py-2 px-4 border border-gray-300 rounded-md shadow-sm text-sm font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                          Cancel
                        </button>
                        <a href="/onboarding/guardians_add" type="submit" class="ml-3 inline-flex justify-center py-2 px-4 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                          Save
                        </a>
                      </div>
                    </div>
                </x-slot>
            </div>
          </div>
        </main>
    </div>
</x-app-layout>