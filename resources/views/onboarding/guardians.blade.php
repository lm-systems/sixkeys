<x-app-layout>
  <x-slot name="module">
    {{ __('secure-enclave') }}
  </x-slot>
    <div class="grid grid-cols-2 md:grid-cols-2 grid-rows-2 md:grid-rows-2 w-11/12 mx-auto">

      <main class="flex-1 relative z-0 overflow-y-auto focus:outline-none">
          <div class="py-6">
            <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <x-slot name="header">
                    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                        {{ __('Getting Started') }}
                    </h2>
                </x-slot>
            </div>
            <div class="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
                <x-slot name="content">
                    <div class="lg:grid lg:grid-cols-12 lg:gap-x-5">
                    <aside class="py-6 px-2 sm:px-6 lg:py-0 lg:px-0 lg:col-span-3">
                      <nav aria-label="Progress">
                        <ol class="overflow-hidden">
                          <li class="relative pb-10">
                            <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-indigo-600" aria-hidden="true"></div>
                            <!-- Complete Step -->
                            <a href="/onboarding/personal" class="relative flex items-start group">
                            <span class="h-9 flex items-center">
                              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-indigo-600 rounded-full group-hover:bg-indigo-800">
                                <!-- Heroicon name: solid/check -->
                                <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg>
                              </span>
                            </span>
                            <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-indigo-600">Personal Information</span>
                                <span class="text-sm text-gray-500">Cursus semper viverra facilisis et et some more.</span>
                              </span>
                            </a>
                          </li>

                          <li class="relative pb-10">
                            <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-indigo-600" aria-hidden="true"></div>
                            <!-- Complete Step -->
                            <a href="/onboarding/beneficiaries" class="relative flex items-start group">
                            <span class="h-9 flex items-center">
                              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-indigo-600 rounded-full group-hover:bg-indigo-800">
                                <!-- Heroicon name: solid/check -->
                                <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg>
                              </span>
                            </span>
                              <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Beneficiaries</span>
                                <span class="text-sm text-gray-500">Penatibus eu quis ante.</span>
                              </span>
                            </a>
                          </li>

                          <li class="relative pb-10">
                            <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-indigo-600" aria-hidden="true"></div>
                            <!-- Complete Step -->
                            <a href="/onboarding/finances" class="relative flex items-start group">
                            <span class="h-9 flex items-center">
                              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-indigo-600 rounded-full group-hover:bg-indigo-800">
                                <!-- Heroicon name: solid/check -->
                                <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg>
                              </span>
                            </span>
                              <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Finances</span>
                                <span class="text-sm text-gray-500">Penatibus eu quis ante.</span>
                              </span>
                            </a>
                          </li>

                          <li class="relative pb-10">
                            <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-indigo-600" aria-hidden="true"></div>
                            <!-- Complete Step -->
                            <a href="/onboarding/will" class="relative flex items-start group">
                            <span class="h-9 flex items-center">
                              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-indigo-600 rounded-full group-hover:bg-indigo-800">
                                <!-- Heroicon name: solid/check -->
                                <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg>
                              </span>
                            </span>
                              <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Will and Executor</span>
                                <span class="text-sm text-gray-500">Faucibus nec enim leo et.</span>
                              </span>
                            </a>
                          </li>

                          <li class="relative pb-10">
                            <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-indigo-600" aria-hidden="true"></div>
                            <!-- Complete Step -->
                            <a href="/onboarding/physical" class="relative flex items-start group">
                            <span class="h-9 flex items-center">
                              <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-indigo-600 rounded-full group-hover:bg-indigo-800">
                                <!-- Heroicon name: solid/check -->
                                <svg class="w-5 h-5 text-white" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                  <path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd" />
                                </svg>
                              </span>
                            </span>
                              <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Physical Assets</span>
                                <span class="text-sm text-gray-500">Faucibus nec enim leo et.</span>
                              </span>
                            </a>
                          </li>

                          <li class="relative pb-10">
                            <div class="-ml-px absolute mt-0.5 top-4 left-4 w-0.5 h-full bg-gray-300" aria-hidden="true"></div>
                            <!-- Upcoming Step -->
                            <a href="/onboarding/guardians" class="relative flex items-start group" aria-current="step">
                              <span class="h-9 flex items-center" aria-hidden="true">
                                <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-indigo-600 rounded-full">
                                  <span class="h-2.5 w-2.5 bg-indigo-600 rounded-full"></span>
                                </span>
                              </span>
                              <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Guardians</span>
                                <span class="text-sm text-gray-500">Assign up to 5 Guardians to protect your vault.</span>
                              </span>
                            </a>
                          </li>

                          <li class="relative">
                            <!-- Upcoming Step -->
                            <a href="/onboarding/finish" class="relative flex items-start group">
                              <span class="h-9 flex items-center" aria-hidden="true">
                                <span class="relative z-10 w-8 h-8 flex items-center justify-center bg-white border-2 border-gray-300 rounded-full group-hover:border-gray-400">
                                  <span class="h-2.5 w-2.5 bg-transparent rounded-full group-hover:bg-gray-300"></span>
                                </span>
                              </span>
                              <span class="ml-4 min-w-0 flex flex-col">
                                <span class="text-xs font-semibold tracking-wide uppercase text-gray-500">Finish</span>
                                <span class="text-sm text-gray-500">Review your information and finish the getting started wizard.</span>
                              </span>
                            </a>
                          </li>
                          
                        </ol>
                      </nav>
                    </aside>
                    <div class="space-y-6 sm:px-6 lg:px-0 lg:col-span-9">
                      <div class="bg-white overflow-hidden shadow rounded-lg divide-y divide-gray-200">
                        <div class="">
                          <div class="bg-white px-4 py-5 sm:px-6">
                            <div class="-ml-4 -mt-4 flex justify-between items-center flex-wrap sm:flex-nowrap">
                              <div class="ml-4 mt-4">
                                <h3 class="text-lg leading-6 font-medium text-gray-900">
                                  Guardians (3/5)
                                </h3>
                                <p class="mt-1 text-sm text-gray-500">
                                  Lorem ipsum dolor sit amet consectetur adipisicing elit quam corrupti consectetur.
                                </p>
                              </div>
                              <div class="ml-4 mt-4 flex-shrink-0">
                                <a href="/onboarding/guardians_add" type="button" class="relative inline-flex items-center px-4 py-2 border border-transparent shadow-sm text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                                  Add new guardian
                                </a>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="">
                          <div class="bg-white shadow overflow-hidden sm:rounded-md">
                            <ul class="divide-y divide-gray-200">
                              <li>
                                <a href="#" class="block hover:bg-gray-50">
                                  <div class="flex items-center px-4 py-4 sm:px-6">
                                    <div class="min-w-0 flex-1 flex items-center">
                                      <div class="flex-shrink-0">
                                        <img class="h-12 w-12 rounded-full" src="https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixqx=meBL8MvP9Y&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
                                      </div>
                                      <div class="min-w-0 flex-1 px-4 md:grid md:grid-cols-2 md:gap-4">
                                        <div>
                                          <p class="text-sm font-medium text-indigo-600 truncate">Ricardo Cooper</p>
                                          <p class="mt-2 flex items-center text-sm text-gray-500">
                                            <!-- Heroicon name: solid/mail -->
                                            <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                              <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z" />
                                              <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z" />
                                            </svg>
                                            <span class="truncate">ricardo.cooper@example.com</span>
                                          </p>
                                        </div>
                                        <div class="hidden md:block">
                                          <div>
                                            <p class="text-sm text-gray-900">
                                              Accepted on
                                              <time datetime="2020-01-07">January 7, 2020</time>
                                            </p>
                                            <p class="mt-2 flex items-center text-sm text-gray-500">
                                              <!-- Heroicon name: solid/check-circle -->
                                              <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-green-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                                              </svg>
                                              Completed sign up
                                            </p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div>
                                      <!-- Heroicon name: solid/chevron-right -->
                                      <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                                      </svg>
                                    </div>
                                  </div>
                                </a>
                              </li>

                              <li>
                                <a href="#" class="block hover:bg-gray-50">
                                  <div class="flex items-center px-4 py-4 sm:px-6">
                                    <div class="min-w-0 flex-1 flex items-center">
                                      <div class="flex-shrink-0">
                                        <img class="h-12 w-12 rounded-full" src="https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&ixqx=meBL8MvP9Y&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
                                      </div>
                                      <div class="min-w-0 flex-1 px-4 md:grid md:grid-cols-2 md:gap-4">
                                        <div>
                                          <p class="text-sm font-medium text-indigo-600 truncate">Kristen Ramos</p>
                                          <p class="mt-2 flex items-center text-sm text-gray-500">
                                            <!-- Heroicon name: solid/mail -->
                                            <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                              <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z" />
                                              <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z" />
                                            </svg>
                                            <span class="truncate">kristen.ramos@example.com</span>
                                          </p>
                                        </div>
                                        <div class="hidden md:block">
                                          <div>
                                            <p class="text-sm text-gray-900">
                                              Accepted on
                                              <time datetime="2020-01-07">January 7, 2020</time>
                                            </p>
                                            <p class="mt-2 flex items-center text-sm text-gray-500">
                                              <!-- Heroicon name: solid/check-circle -->
                                              <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-green-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                                              </svg>
                                              Completed sign up
                                            </p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div>
                                      <!-- Heroicon name: solid/chevron-right -->
                                      <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                                      </svg>
                                    </div>
                                  </div>
                                </a>
                              </li>

                              <li>
                                <a href="#" class="block hover:bg-gray-50">
                                  <div class="flex items-center px-4 py-4 sm:px-6">
                                    <div class="min-w-0 flex-1 flex items-center">
                                      <div class="flex-shrink-0">
                                        <img class="h-12 w-12 rounded-full" src="https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixqx=meBL8MvP9Y&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
                                      </div>
                                      <div class="min-w-0 flex-1 px-4 md:grid md:grid-cols-2 md:gap-4">
                                        <div>
                                          <p class="text-sm font-medium text-indigo-600 truncate">Ted Fox</p>
                                          <p class="mt-2 flex items-center text-sm text-gray-500">
                                            <!-- Heroicon name: solid/mail -->
                                            <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                              <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z" />
                                              <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z" />
                                            </svg>
                                            <span class="truncate">ted.fox@example.com</span>
                                          </p>
                                        </div>
                                        <div class="hidden md:block">
                                          <div>
                                            <p class="text-sm text-gray-900">
                                              Accepted on
                                              <time datetime="2020-01-07">January 7, 2020</time>
                                            </p>
                                            <p class="mt-2 flex items-center text-sm text-gray-500">
                                              <!-- Heroicon name: solid/check-circle -->
                                              <svg class="flex-shrink-0 mr-1.5 h-5 w-5 text-green-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                                <path fill-rule="evenodd" d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z" clip-rule="evenodd" />
                                              </svg>
                                              Completed sign up
                                            </p>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div>
                                      <!-- Heroicon name: solid/chevron-right -->
                                      <svg class="h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                        <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd" />
                                      </svg>
                                    </div>
                                  </div>
                                </a>
                              </li>
                            </ul>
                          </div>
                        </div>
                      </div>
                      <div class="flex justify-end px-5 gap-4">
                        <a href="/onboarding/physical" type="button" class="inline-flex items-center px-4 py-2 border border-transparent text-base font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                          Back
                        </a>
                        <a href="/onboarding/finish" type="button" class="inline-flex items-center px-4 py-2 border border-transparent text-base font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                          Next
                        </a>
                      </div>
                    </div>

                  </div>
                </x-slot>
            </div>
          </div>
        </main>
    </div>
</x-app-layout>