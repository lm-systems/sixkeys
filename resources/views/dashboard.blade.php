<x-app-layout>
  <x-slot name="module">
    {{ __('secure-enclave') }}
  </x-slot>
    <div class="grid grid-cols-2 md:grid-cols-2 grid-rows-2 md:grid-rows-2 w-11/12 mx-auto">

      <main class="flex-1 relative z-0 overflow-y-auto focus:outline-none">
          <div class="py-6">
            <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <x-slot name="header">
                    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                        {{ __('Welcome Bruce,') }}
                    </h2>
                </x-slot>
            </div>
            <div class="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
              <x-slot name="content">
                    <!-- This example requires Tailwind CSS v2.0+ -->
                <div class="bg-grey overflow-hidden shadow rounded-lg">
                  <div class="px-4 py-5 sm:p-6">
                    <p class="pb-5">Need help getting started? Try our new getting started wizard below.</p>
                    <a href="/onboarding/start" type="button" class="inline-flex items-center px-4 py-2 border border-transparent text-base font-medium rounded-md shadow-sm text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500">
                      Get Started
                    </a>
                  </div>
                </div>

                <div class="py-5">
                    <h2>Manage your vault</h2>
                </div>

                <div class="grid grid-cols-4 gap-4">
                    @livewire('category-panel', ['panel_title' => 'Personal Information', 'percentage' => '100', 'path' => 'M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z','link' => 'personal-information', 'completed' => 'true'])
                    @livewire('category-panel', ['panel_title' => 'Beneficiaries', 'percentage' => '60', 'path' => 'M17 20h5v-2a3 3 0 00-5.356-1.857M17 20H7m10 0v-2c0-.656-.126-1.283-.356-1.857M7 20H2v-2a3 3 0 015.356-1.857M7 20v-2c0-.656.126-1.283.356-1.857m0 0a5.002 5.002 0 019.288 0M15 7a3 3 0 11-6 0 3 3 0 016 0zm6 3a2 2 0 11-4 0 2 2 0 014 0zM7 10a2 2 0 11-4 0 2 2 0 014 0z','link' => 'beneficiaries', 'completed' => 'false'])
                    @livewire('category-panel', ['panel_title' => 'Finance', 'percentage' => '45', 'path' => 'M15 9a2 2 0 10-4 0v5a2 2 0 01-2 2h6m-6-4h4m8 0a9 9 0 11-18 0 9 9 0 0118 0z','link' => 'finances', 'completed' => 'false'])
                    @livewire('category-panel', ['panel_title' => 'Financial Advisors', 'percentage' => '80', 'path' => 'M8 14v3m4-3v3m4-3v3M3 21h18M3 10h18M3 7l9-4 9 4M4 10h16v11H4V10z','link' => 'financial-advisors', 'completed' => 'false'])
                    @livewire('category-panel', ['panel_title' => 'Will and Executors', 'percentage' => '80', 'path' => 'M9 5H7a2 2 0 00-2 2v12a2 2 0 002 2h10a2 2 0 002-2V7a2 2 0 00-2-2h-2M9 5a2 2 0 002 2h2a2 2 0 002-2M9 5a2 2 0 012-2h2a2 2 0 012 2m-6 9l2 2 4-4','link' => 'will-executors', 'completed' => 'false'])
                    @livewire('category-panel', ['panel_title' => 'Physical Assets', 'percentage' => '80', 'path' => 'M15 7a2 2 0 012 2m4 0a6 6 0 01-7.743 5.743L11 17H9v2H7v2H4a1 1 0 01-1-1v-2.586a1 1 0 01.293-.707l5.964-5.964A6 6 0 1121 9z','link' => 'physical-assets', 'completed' => 'false'])
                    @livewire('category-panel', ['panel_title' => 'Business Information', 'percentage' => '45', 'path' => 'M21 13.255A23.931 23.931 0 0112 15c-3.183 0-6.22-.62-9-1.745M16 6V4a2 2 0 00-2-2h-4a2 2 0 00-2 2v2m4 6h.01M5 20h14a2 2 0 002-2V8a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z','link' => 'business-information', 'completed' => 'false'])
                    @livewire('category-panel', ['panel_title' => 'Lifecycle Documents', 'percentage' => '45', 'path' => 'M9 12h6m-6 4h6m2 5H7a2 2 0 01-2-2V5a2 2 0 012-2h5.586a1 1 0 01.707.293l5.414 5.414a1 1 0 01.293.707V19a2 2 0 01-2 2z','link' => 'lifecycle-documents', 'completed' => 'false'])
                    @livewire('category-panel', ['panel_title' => 'Digital World', 'percentage' => '45', 'path' => 'M21 12a9 9 0 01-9 9m9-9a9 9 0 00-9-9m9 9H3m9 9a9 9 0 01-9-9m9 9c1.657 0 3-4.03 3-9s-1.343-9-3-9m0 18c-1.657 0-3-4.03-3-9s1.343-9 3-9m-9 9a9 9 0 019-9','link' => 'digital-world', 'completed' => 'false'])
                    @livewire('category-panel', ['panel_title' => 'Subscriptions/Memberships', 'percentage' => '45', 'path' => 'M15 5v2m0 4v2m0 4v2M5 5a2 2 0 00-2 2v3a2 2 0 110 4v3a2 2 0 002 2h14a2 2 0 002-2v-3a2 2 0 110-4V7a2 2 0 00-2-2H5z','link' => 'subscriptions-memberships', 'completed' => 'false'])
                    @livewire('category-panel', ['panel_title' => 'Health Information', 'percentage' => '45', 'path' => 'M4.318 6.318a4.5 4.5 0 000 6.364L12 20.364l7.682-7.682a4.5 4.5 0 00-6.364-6.364L12 7.636l-1.318-1.318a4.5 4.5 0 00-6.364 0z','link' => 'health-information', 'completed' => 'false'])
                    @livewire('category-panel', ['panel_title' => 'Requests for when I die', 'percentage' => '45', 'path' => 'M20.354 15.354A9 9 0 018.646 3.646 9.003 9.003 0 0012 21a9.003 9.003 0 008.354-5.646z','link' => 'requests'])
                    @livewire('category-panel', ['panel_title' => 'House', 'percentage' => '45', 'path' => 'M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6','link' => 'house'])
                    @livewire('category-panel', ['panel_title' => 'Information for loved ones', 'percentage' => '45', 'path' => 'M12 8v13m0-13V6a2 2 0 112 2h-2zm0 0V5.5A2.5 2.5 0 109.5 8H12zm-7 4h14M5 12a2 2 0 110-4h14a2 2 0 110 4M5 12v7a2 2 0 002 2h10a2 2 0 002-2v-7','link' => 'information-loved-ones'])

                    
                </div>
                

                </x-slot>
            </div>
          </div>
        </main>
    </div>
</x-app-layout>