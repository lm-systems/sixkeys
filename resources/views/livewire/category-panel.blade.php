<a href="/{{ $link }}">
    @if( $completed  =='true')
        <div class="bg-grey border border-green-300 overflow-hidden shadow rounded-lg">
            <div class="px-4 py-5 sm:p-6">  
                <svg class="mx-auto text-green-400 group-hover:text-green-500 h-24 w-24" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="#34D399" aria-hidden="true">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="{{ $path }}" />
                </svg>
                <div class="text-center text-base font-bold text-green-400">
                    {{ $panel_title }}
                </div>
                <div class="relative px-4 pt-1">
                  <div class="overflow-hidden h-2 mb-4 text-xs flex rounded bg-green-500">
                    <div style="width:{{ $percentage }}%" class="shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-green-400"></div>
                  </div>
                </div>
                <div class="text-center text-sm text-green-400">
                    Completed
                </div> 
            </div>
        </div>
    @else
        <div class="bg-grey overflow-hidden shadow rounded-lg">
            <div class="px-4 py-5 sm:p-6">  
                <svg class="mx-auto text-gray-400 group-hover:text-gray-300 h-24 w-24" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="#BAAD88" aria-hidden="true">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="{{ $path }}" />
                </svg>
                <div class="text-center text-base font-bold text-Mongoose">
                    {{ $panel_title }}
                </div>
                <div class="relative px-4 pt-1">
                  <div class="overflow-hidden h-2 mb-4 text-xs flex rounded bg-Sisal">
                    <div style="width:{{ $percentage }}%" class="shadow-none flex flex-col text-center whitespace-nowrap text-white justify-center bg-Mongoose"></div>
                  </div>
                </div>
                <div class="text-center text-sm text-Mongoose">
                    {{ $percentage }}% completed
                </div>     
            </div>
        </div>  
    @endif
</a>
