<x-app-layout>
  <x-slot name="module">
    {{ __('secure-enclave') }}
  </x-slot>
    <div class="grid grid-cols-2 md:grid-cols-2 grid-rows-2 md:grid-rows-2 w-11/12 mx-auto">

      <main class="flex-1 relative z-0 overflow-y-auto focus:outline-none">
          <div class="py-6">
            <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <x-slot name="header">
                    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                        {{ __('') }}
                    </h2>
                </x-slot>
            </div>
            <div class="max-w-7xl mx-auto">
                <x-slot name="content">
                    <article>
          <!-- Profile header -->
                    <div>
                      <div>
                        <img class="h-32 w-full object-cover lg:h-48" src="https://images.unsplash.com/photo-1444628838545-ac4016a5418a?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80" alt="">
                      </div>
                      <div class="max-w-5xl mx-auto px-4 sm:px-6 lg:px-8">
                        <div class="-mt-12 sm:-mt-16 sm:flex sm:items-end sm:space-x-5">
                          <div class="flex">
                            <img class="h-24 w-24 rounded-full ring-4 ring-white sm:h-32 sm:w-32" src="http://movie-locations.com/movies/b/Brazil-Mentmore-Towers.jpg" alt="">
                          </div>
                          <div class="mt-6 sm:flex-1 sm:min-w-0 sm:flex sm:items-center sm:justify-end sm:space-x-6 sm:pb-1">
                            <div class="sm:hidden 2xl:block mt-6 min-w-0 flex-1">
                              <h1 class="text-2xl font-bold text-gray-900 truncate">
                                Primary Residence
                              </h1>
                            </div>
                            <div class="mt-6 flex flex-col justify-stretch space-y-3 sm:flex-row sm:space-y-0 sm:space-x-4">
                              {{-- <button type="button" class="inline-flex justify-center px-4 py-2 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-pink-500">
                                <!-- Heroicon name: solid/mail -->
                                <svg class="-ml-1 mr-2 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                  <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z" />
                                  <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z" />
                                </svg>
                                <span>Message</span>
                              </button>
                              <button type="button" class="inline-flex justify-center px-4 py-2 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-pink-500">
                                <!-- Heroicon name: solid/phone -->
                                <svg class="-ml-1 mr-2 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                  <path d="M2 3a1 1 0 011-1h2.153a1 1 0 01.986.836l.74 4.435a1 1 0 01-.54 1.06l-1.548.773a11.037 11.037 0 006.105 6.105l.774-1.548a1 1 0 011.059-.54l4.435.74a1 1 0 01.836.986V17a1 1 0 01-1 1h-2C7.82 18 2 12.18 2 5V3z" />
                                </svg>
                                <span>Call</span>
                              </button> --}}
                            </div>
                          </div>
                        </div>
                        <div class="hidden sm:block 2xl:hidden mt-6 min-w-0 flex-1">
                          <h1 class="text-2xl font-bold text-gray-900 truncate">
                            Primary Residence
                          </h1>
                        </div>
                      </div>
                    </div>

                    <!-- Tabs -->
                    <div class="mt-6 sm:mt-2 2xl:mt-5">
                      <div class="border-b border-gray-200">
                        <div class="max-w-5xl mx-auto px-4 sm:px-6 lg:px-8">
                          <nav class="-mb-px flex space-x-8" aria-label="Tabs">
                            <!-- Current: "border-pink-500 text-gray-900", Default: "border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300" -->
                            <a href="/physical-assets/property/property_show" class="border-transparent text-gray-500 whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm" aria-current="page">
                              Details
                            </a>

                            <a href="/physical-assets/property/property_photos" class="border-pink-500 text-gray-900 hover:text-gray-700 hover:border-gray-300 whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm">
                              Photos
                            </a>

                            <a href="/physical-assets/property/property_notes" class="border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300 whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm">
                              Notes
                            </a>
                          </nav>
                        </div>
                      </div>
                    </div>

                    <!-- Description list -->
                    <div class="p-3"> 
                      <ul role="list" class="grid grid-cols-2 gap-x-4 gap-y-8 sm:grid-cols-3 sm:gap-x-6 lg:grid-cols-4 xl:gap-x-8">
                        <li class="relative">
                          <div class="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
                            <img src="https://static.independent.co.uk/s3fs-public/thumbnails/image/2012/07/30/15/wayne.jpg?width=1200" alt="" class="object-cover pointer-events-none group-hover:opacity-75">
                            <button type="button" class="absolute inset-0 focus:outline-none">
                              <span class="sr-only">View details for IMG_4985.HEIC</span>
                            </button>
                          </div>
                          <p class="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">IMG_4985.HEIC</p>
                          <p class="block text-sm font-medium text-gray-500 pointer-events-none">3.9 MB</p>
                        </li>
                        <li class="relative">
                          <div class="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
                            <img src="https://i.stack.imgur.com/0d6kt.jpg" alt="" class="object-cover pointer-events-none group-hover:opacity-75">
                            <button type="button" class="absolute inset-0 focus:outline-none">
                              <span class="sr-only">View details for IMG_4985.HEIC</span>
                            </button>
                          </div>
                          <p class="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">IMG_4985.HEIC</p>
                          <p class="block text-sm font-medium text-gray-500 pointer-events-none">3.9 MB</p>
                        </li>
                        <li class="relative">
                          <div class="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
                            <img src="https://i.pinimg.com/originals/6f/c0/f5/6fc0f5c92657a21b825afa8c106b6589.jpg" alt="" class="object-cover pointer-events-none group-hover:opacity-75">
                            <button type="button" class="absolute inset-0 focus:outline-none">
                              <span class="sr-only">View details for IMG_4985.HEIC</span>
                            </button>
                          </div>
                          <p class="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">IMG_4985.HEIC</p>
                          <p class="block text-sm font-medium text-gray-500 pointer-events-none">3.9 MB</p>
                        </li>
                        <li class="relative">
                          <div class="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
                            <img src="https://media.architecturaldigest.com/photos/55e77636302ba71f301700fb/master/w_900,h_600,c_limit/dam-images-daily-2014-09-gotham-sets-gotham-set-design-02-wayne-manor-library.jpg" alt="" class="object-cover pointer-events-none group-hover:opacity-75">
                            <button type="button" class="absolute inset-0 focus:outline-none">
                              <span class="sr-only">View details for IMG_4985.HEIC</span>
                            </button>
                          </div>
                          <p class="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">IMG_4985.HEIC</p>
                          <p class="block text-sm font-medium text-gray-500 pointer-events-none">3.9 MB</p>
                        </li>
                        <li class="relative">
                          <div class="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
                            <img src="https://gothamtrending.files.wordpress.com/2011/09/wayne-manor-entrance-interiors-osterly-park-the-dark-knight-rises.jpg?w=584" alt="" class="object-cover pointer-events-none group-hover:opacity-75">
                            <button type="button" class="absolute inset-0 focus:outline-none">
                              <span class="sr-only">View details for IMG_4985.HEIC</span>
                            </button>
                          </div>
                          <p class="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">IMG_4985.HEIC</p>
                          <p class="block text-sm font-medium text-gray-500 pointer-events-none">3.9 MB</p>
                        </li>
                        <li class="relative">
                          <div class="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
                            <img src="https://www.movie-locations.com/movies/b/Batman-1989-Library-Hatfield-House.jpg" alt="" class="object-cover pointer-events-none group-hover:opacity-75">
                            <button type="button" class="absolute inset-0 focus:outline-none">
                              <span class="sr-only">View details for IMG_4985.HEIC</span>
                            </button>
                          </div>
                          <p class="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">IMG_4985.HEIC</p>
                          <p class="block text-sm font-medium text-gray-500 pointer-events-none">3.9 MB</p>
                        </li>
                        <li class="relative">
                          <div class="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
                            <img src="https://i.pinimg.com/originals/96/d3/6a/96d36a2909f43552c1aad75d411712a6.jpg" alt="" class="object-cover pointer-events-none group-hover:opacity-75">
                            <button type="button" class="absolute inset-0 focus:outline-none">
                              <span class="sr-only">View details for IMG_4985.HEIC</span>
                            </button>
                          </div>
                          <p class="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">IMG_4985.HEIC</p>
                          <p class="block text-sm font-medium text-gray-500 pointer-events-none">3.9 MB</p>
                        </li>
                        <li class="relative">
                          <div class="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
                            <img src="https://pbs.twimg.com/media/DonAdq5XsAERh_8.jpg" alt="" class="object-cover pointer-events-none group-hover:opacity-75">
                            <button type="button" class="absolute inset-0 focus:outline-none">
                              <span class="sr-only">View details for IMG_4985.HEIC</span>
                            </button>
                          </div>
                          <p class="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">IMG_4985.HEIC</p>
                          <p class="block text-sm font-medium text-gray-500 pointer-events-none">3.9 MB</p>
                        </li>

                        <!-- More files... -->
                      </ul>
                    </div>

   
                    
                  </article>
                </x-slot>
            </div>
          </div>
        </main>
    </div>
</x-app-layout>