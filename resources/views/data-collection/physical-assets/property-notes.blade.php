<x-app-layout>
  <x-slot name="module">
    {{ __('secure-enclave') }}
  </x-slot>
    <div class="grid grid-cols-2 md:grid-cols-2 grid-rows-2 md:grid-rows-2 w-11/12 mx-auto">

      <main class="flex-1 relative z-0 overflow-y-auto focus:outline-none">
          <div class="py-6">
            <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <x-slot name="header">
                    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                        {{ __('') }}
                    </h2>
                </x-slot>
            </div>
            <div class="max-w-7xl mx-auto">
                <x-slot name="content">
                    <article>
          <!-- Profile header -->
                    <div>
                      <div>
                        <img class="h-32 w-full object-cover lg:h-48" src="https://images.unsplash.com/photo-1444628838545-ac4016a5418a?ixid=MXwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=1950&q=80" alt="">
                      </div>
                      <div class="max-w-5xl mx-auto px-4 sm:px-6 lg:px-8">
                        <div class="-mt-12 sm:-mt-16 sm:flex sm:items-end sm:space-x-5">
                          <div class="flex">
                            <img class="h-24 w-24 rounded-full ring-4 ring-white sm:h-32 sm:w-32" src="http://movie-locations.com/movies/b/Brazil-Mentmore-Towers.jpg" alt="">
                          </div>
                          <div class="mt-6 sm:flex-1 sm:min-w-0 sm:flex sm:items-center sm:justify-end sm:space-x-6 sm:pb-1">
                            <div class="sm:hidden 2xl:block mt-6 min-w-0 flex-1">
                              <h1 class="text-2xl font-bold text-gray-900 truncate">
                                Primary Residence
                              </h1>
                            </div>
                            <div class="mt-6 flex flex-col justify-stretch space-y-3 sm:flex-row sm:space-y-0 sm:space-x-4">
                              {{-- <button type="button" class="inline-flex justify-center px-4 py-2 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-pink-500">
                                <!-- Heroicon name: solid/mail -->
                                <svg class="-ml-1 mr-2 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                  <path d="M2.003 5.884L10 9.882l7.997-3.998A2 2 0 0016 4H4a2 2 0 00-1.997 1.884z" />
                                  <path d="M18 8.118l-8 4-8-4V14a2 2 0 002 2h12a2 2 0 002-2V8.118z" />
                                </svg>
                                <span>Message</span>
                              </button>
                              <button type="button" class="inline-flex justify-center px-4 py-2 border border-gray-300 shadow-sm text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-pink-500">
                                <!-- Heroicon name: solid/phone -->
                                <svg class="-ml-1 mr-2 h-5 w-5 text-gray-400" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor" aria-hidden="true">
                                  <path d="M2 3a1 1 0 011-1h2.153a1 1 0 01.986.836l.74 4.435a1 1 0 01-.54 1.06l-1.548.773a11.037 11.037 0 006.105 6.105l.774-1.548a1 1 0 011.059-.54l4.435.74a1 1 0 01.836.986V17a1 1 0 01-1 1h-2C7.82 18 2 12.18 2 5V3z" />
                                </svg>
                                <span>Call</span>
                              </button> --}}
                            </div>
                          </div>
                        </div>
                        <div class="hidden sm:block 2xl:hidden mt-6 min-w-0 flex-1">
                          <h1 class="text-2xl font-bold text-gray-900 truncate">
                            Primary Residence
                          </h1>
                        </div>
                      </div>
                    </div>

                    <!-- Tabs -->
                    <div class="mt-6 sm:mt-2 2xl:mt-5">
                      <div class="border-b border-gray-200">
                        <div class="max-w-5xl mx-auto px-4 sm:px-6 lg:px-8">
                          <nav class="-mb-px flex space-x-8" aria-label="Tabs">
                            <!-- Current: "border-pink-500 text-gray-900", Default: "border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300" -->
                            <a href="/physical-assets/property/property_show" class="border-transparent text-gray-500 whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm" aria-current="page">
                              Details
                            </a>

                            <a href="/physical-assets/property/property_photos" class="border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300 whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm">
                              Photos
                            </a>

                            <a href="/physical-assets/property/property_notes" class="border-pink-500 text-gray-900 hover:text-gray-700 hover:border-gray-300 whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm">
                              Notes
                            </a>
                          </nav>
                        </div>
                      </div>
                    </div>

                    <!-- Description list -->
                    <div class="p-3"> 
                      <!-- Comments-->
                      <section aria-labelledby="notes-title">
                        <div class="bg-white shadow sm:rounded-lg sm:overflow-hidden">
                          <div class="divide-y divide-gray-200">
                            <div class="px-4 py-5 sm:px-6">
                              <h2 id="notes-title" class="text-lg font-medium text-gray-900">Notes</h2>
                            </div>
                            <div class="px-4 py-6 sm:px-6">
                              <ul class="space-y-8">
                                <li>
                                  <div class="flex space-x-3">
                                    <div class="flex-shrink-0">
                                      <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1494790108377-be9c29b29330?ixlib=rb-1.2.1&ixqx=meBL8MvP9Y&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
                                    </div>
                                    <div>
                                      <div class="text-sm">
                                        <a href="#" class="font-medium text-gray-900">Leslie Alexander</a>
                                      </div>
                                      <div class="mt-1 text-sm text-gray-700">
                                        <p>Ducimus quas delectus ad maxime totam doloribus reiciendis ex. Tempore dolorem maiores. Similique voluptatibus tempore non ut.</p>
                                      </div>
                                      <div class="mt-2 text-sm space-x-2">
                                        <span class="text-gray-500 font-medium">4d ago</span>
                                        <span class="text-gray-500 font-medium">&middot;</span>
                                        <button type="button" class="text-gray-900 font-medium">Reply</button>
                                      </div>
                                    </div>
                                  </div>
                                </li>

                                <li>
                                  <div class="flex space-x-3">
                                    <div class="flex-shrink-0">
                                      <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1519244703995-f4e0f30006d5?ixlib=rb-1.2.1&ixqx=meBL8MvP9Y&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
                                    </div>
                                    <div>
                                      <div class="text-sm">
                                        <a href="#" class="font-medium text-gray-900">Michael Foster</a>
                                      </div>
                                      <div class="mt-1 text-sm text-gray-700">
                                        <p>Et ut autem. Voluptatem eum dolores sint necessitatibus quos. Quis eum qui dolorem accusantium voluptas voluptatem ipsum. Quo facere iusto quia accusamus veniam id explicabo et aut.</p>
                                      </div>
                                      <div class="mt-2 text-sm space-x-2">
                                        <span class="text-gray-500 font-medium">4d ago</span>
                                        <span class="text-gray-500 font-medium">&middot;</span>
                                        <button type="button" class="text-gray-900 font-medium">Reply</button>
                                      </div>
                                    </div>
                                  </div>
                                </li>

                                <li>
                                  <div class="flex space-x-3">
                                    <div class="flex-shrink-0">
                                      <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1506794778202-cad84cf45f1d?ixlib=rb-1.2.1&ixqx=meBL8MvP9Y&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
                                    </div>
                                    <div>
                                      <div class="text-sm">
                                        <a href="#" class="font-medium text-gray-900">Dries Vincent</a>
                                      </div>
                                      <div class="mt-1 text-sm text-gray-700">
                                        <p>Expedita consequatur sit ea voluptas quo ipsam recusandae. Ab sint et voluptatem repudiandae voluptatem et eveniet. Nihil quas consequatur autem. Perferendis rerum et.</p>
                                      </div>
                                      <div class="mt-2 text-sm space-x-2">
                                        <span class="text-gray-500 font-medium">4d ago</span>
                                        <span class="text-gray-500 font-medium">&middot;</span>
                                        <button type="button" class="text-gray-900 font-medium">Reply</button>
                                      </div>
                                    </div>
                                  </div>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <div class="bg-gray-50 px-4 py-6 sm:px-6">
                            <div class="flex space-x-3">
                              <div class="flex-shrink-0">
                                <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1517365830460-955ce3ccd263?ixlib=rb-=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=8&w=256&h=256&q=80" alt="">
                              </div>
                              <div class="min-w-0 flex-1">
                                <form action="#">
                                  <div>
                                    <label for="comment" class="sr-only">About</label>
                                    <textarea id="comment" name="comment" rows="3" class="shadow-sm block w-full focus:ring-blue-500 focus:border-blue-500 sm:text-sm border-gray-300 rounded-md" placeholder="Add a note"></textarea>
                                  </div>
                                  <div class="mt-3 flex items-center justify-between">
                                    <a href="#" class="group inline-flex items-start text-sm space-x-2 text-gray-500 hover:text-gray-900">
                                      <!-- Heroicon name: solid/question-mark-circle -->
                                      
                                    </a>
                                    <button type="submit" class="inline-flex items-center justify-center px-4 py-2 border border-transparent text-sm font-medium rounded-md shadow-sm text-white bg-blue-600 hover:bg-blue-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-blue-500">
                                      Add note
                                    </button>
                                  </div>
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </section>
                    </div>

   
                    
                  </article>
                </x-slot>
            </div>
          </div>
        </main>
    </div>
</x-app-layout>