<x-app-layout>
    <x-slot name="module">
        {{ __('secure-enclave') }}
    </x-slot>
    <div class="grid grid-cols-2 md:grid-cols-2 grid-rows-2 md:grid-rows-2 w-11/12 mx-auto">

      <main class="flex-1 relative z-0 overflow-y-auto focus:outline-none">
          <div class="py-6">
            <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <x-slot name="header">
                    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                        {{ __('Information for loved ones') }}
                    </h2>
                </x-slot>
            </div>
            <div class="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
                <x-slot name="content">
                   <div class="mt-6 sm:mt-2 2xl:mt-5">
                      <div class="border-b border-gray-200">
                        <div class="max-w-5xl mx-auto px-4 sm:px-6 lg:px-8">
                          <nav class="-mb-px flex space-x-8" aria-label="Tabs">
                            <!-- Current: "border-pink-500 text-gray-900", Default: "border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300" -->
                            <a href="/information-loved-ones" class="border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300 whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm" aria-current="page">
                              Letters for loved ones
                            </a>

                            <a href="/information-loved-ones/memoirs" class="border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300 whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm">
                              My Memoirs
                            </a>

                            <a href="/information-loved-ones/recordings" class="border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300 whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm">
                              Video/Audio Recordings
                            </a>

                            <a href="/information-loved-ones/photos" class="border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300 whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm">
                              Important Photos
                            </a>

                            <a href="/information-loved-ones/family-history" class="border-pink-500 text-gray-900 whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm">
                              Family History
                            </a>
                          </nav>
                        </div>
                      </div>
                    </div> 

                    <main class="lg:col-span-9 xl:col-span-6">
                <div class="mt-4">
                  <ul class="space-y-4">
                    <li class="bg-white px-4 py-6 shadow sm:p-6 sm:rounded-lg">
                      <article aria-labelledby="question-title-81614">
                        <div>
                          <div class="flex space-x-3">
                            <div class="flex-shrink-0">
                              <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1506794778202-cad84cf45f1d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
                            </div>
                            <div class="min-w-0 flex-1">
                              <p class="text-sm font-medium text-gray-900">
                                <a href="#" class="hover:underline">Dries Vincent</a>
                              </p>
                              <p class="text-sm text-gray-500">
                                <a href="#" class="hover:underline">
                                  <time datetime="2020-12-09T11:43:00">December 9 at 11:43 AM</time>
                                </a>
                              </p>
                            </div>
                          </div>
                          <h2 id="question-title-81614" class="mt-4 text-base font-medium text-gray-900">
                            Letter for Jenny
                          </h2>
                        </div>
                        <div class="mt-2 text-sm text-gray-700 space-y-4">
                          <p>
                            I was going through some old boxes the other day and found a box of old holiday photos. I am going to get them digitised and then I will upload them.
                          </p>
                          <p>
                            I remember taking Tom there when he was really little and he always chased the seagulls around. He would run around until he was so tired he could hardly walk.
                          </p>
                        </div>
                        <div class="mt-6 flex justify-between space-x-8">
                
                        </div>
                      </article>
                    </li>
                    <li class="bg-white px-4 py-6 shadow sm:p-6 sm:rounded-lg">
                      <article aria-labelledby="question-title-81614">
                        <div>
                          <div class="flex space-x-3">
                            <div class="flex-shrink-0">
                              <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1506794778202-cad84cf45f1d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
                            </div>
                            <div class="min-w-0 flex-1">
                              <p class="text-sm font-medium text-gray-900">
                                <a href="#" class="hover:underline">Dries Vincent</a>
                              </p>
                              <p class="text-sm text-gray-500">
                                <a href="#" class="hover:underline">
                                  <time datetime="2020-12-09T11:43:00">December 9 at 11:43 AM</time>
                                </a>
                              </p>
                            </div>
                          </div>
                          <h2 id="question-title-81614" class="mt-4 text-base font-medium text-gray-900">
                            Letter for Paul
                          </h2>
                        </div>
                        <div class="mt-2 text-sm text-gray-700 space-y-4">
                          <p>
                            I was going through some old boxes the other day and found a box of old holiday photos. I am going to get them digitised and then I will upload them.
                          </p>
                          <p>
                            I remember taking Tom there when he was really little and he always chased the seagulls around. He would run around until he was so tired he could hardly walk.
                          </p>
                        </div>
                        <div class="mt-6 flex justify-between space-x-8">
                          
                        </div>
                      </article>
                    </li>
                    <li class="bg-white px-4 py-6 shadow sm:p-6 sm:rounded-lg">
                      <article aria-labelledby="question-title-81614">
                        <div>
                          <div class="flex space-x-3">
                            <div class="flex-shrink-0">
                              <img class="h-10 w-10 rounded-full" src="https://images.unsplash.com/photo-1506794778202-cad84cf45f1d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80" alt="">
                            </div>
                            <div class="min-w-0 flex-1">
                              <p class="text-sm font-medium text-gray-900">
                                <a href="#" class="hover:underline">Dries Vincent</a>
                              </p>
                              <p class="text-sm text-gray-500">
                                <a href="#" class="hover:underline">
                                  <time datetime="2020-12-09T11:43:00">December 9 at 11:43 AM</time>
                                </a>
                              </p>
                            </div>
                          </div>
                          <h2 id="question-title-81614" class="mt-4 text-base font-medium text-gray-900">
                            Letter for Sally
                          </h2>
                        </div>
                        <div class="mt-2 text-sm text-gray-700 space-y-4">
                          <p>
                            I was going through some old boxes the other day and found a box of old holiday photos. I am going to get them digitised and then I will upload them.
                          </p>
                          <p>
                            I remember taking Tom there when he was really little and he always chased the seagulls around. He would run around until he was so tired he could hardly walk.
                          </p>
                        </div>
                        <div class="mt-6 flex justify-between space-x-8">
                          
                        </div>
                      </article>
                    </li>
                  </ul>
                </div>
              </main>
                                        
               
                

                </x-slot>
            </div>
          </div>
        </main>
    </div>
</x-app-layout>