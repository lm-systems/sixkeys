<x-app-layout>
    <x-slot name="module">
        {{ __('secure-enclave') }}
    </x-slot>
    <div class="grid grid-cols-2 md:grid-cols-2 grid-rows-2 md:grid-rows-2 w-11/12 mx-auto">

      <main class="flex-1 relative z-0 overflow-y-auto focus:outline-none">
          <div class="py-6">
            <div class="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
                <x-slot name="header">
                    <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                        {{ __('Information for loved ones') }}
                    </h2>
                </x-slot>
            </div>
            <div class="max-w-7xl mx-auto px-4 sm:px-6 md:px-8">
                <x-slot name="content">
                   <div class="mt-6 sm:mt-2 2xl:mt-5">
                      <div class="border-b border-gray-200">
                        <div class="max-w-5xl mx-auto px-4 sm:px-6 lg:px-8">
                          <nav class="-mb-px flex space-x-8" aria-label="Tabs">
                            <!-- Current: "border-pink-500 text-gray-900", Default: "border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300" -->
                            <a href="/information-loved-ones" class="border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300 whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm" aria-current="page">
                              Letters for loved ones
                            </a>

                            <a href="/information-loved-ones/memoirs" class="border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300 whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm">
                              My Memoirs
                            </a>

                            <a href="/information-loved-ones/recordings" class="border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300 whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm">
                              Video/Audio Recordings
                            </a>

                            <a href="/information-loved-ones/photos" class="border-pink-500 text-gray-900 whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm">
                              Important Photos
                            </a>

                            <a href="/information-loved-ones/family-history" class="border-transparent text-gray-500 hover:text-gray-700 hover:border-gray-300 whitespace-nowrap py-4 px-1 border-b-2 font-medium text-sm">
                              Family History
                            </a>
                          </nav>
                        </div>
                      </div>
                    </div> 

                    <div class="p-3"> 
                      <ul role="list" class="grid grid-cols-2 gap-x-4 gap-y-8 sm:grid-cols-3 sm:gap-x-6 lg:grid-cols-4 xl:gap-x-8">
                        <li class="relative">
                          <div class="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
                            <img src="https://static.independent.co.uk/s3fs-public/thumbnails/image/2012/07/30/15/wayne.jpg?width=1200" alt="" class="object-cover pointer-events-none group-hover:opacity-75">
                            <button type="button" class="absolute inset-0 focus:outline-none">
                              <span class="sr-only">View details for IMG_4985.HEIC</span>
                            </button>
                          </div>
                          <p class="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">IMG_4985.HEIC</p>
                          <p class="block text-sm font-medium text-gray-500 pointer-events-none">3.9 MB</p>
                        </li>
                        <li class="relative">
                          <div class="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
                            <img src="https://i.stack.imgur.com/0d6kt.jpg" alt="" class="object-cover pointer-events-none group-hover:opacity-75">
                            <button type="button" class="absolute inset-0 focus:outline-none">
                              <span class="sr-only">View details for IMG_4985.HEIC</span>
                            </button>
                          </div>
                          <p class="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">IMG_4985.HEIC</p>
                          <p class="block text-sm font-medium text-gray-500 pointer-events-none">3.9 MB</p>
                        </li>
                        <li class="relative">
                          <div class="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
                            <img src="https://i.pinimg.com/originals/6f/c0/f5/6fc0f5c92657a21b825afa8c106b6589.jpg" alt="" class="object-cover pointer-events-none group-hover:opacity-75">
                            <button type="button" class="absolute inset-0 focus:outline-none">
                              <span class="sr-only">View details for IMG_4985.HEIC</span>
                            </button>
                          </div>
                          <p class="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">IMG_4985.HEIC</p>
                          <p class="block text-sm font-medium text-gray-500 pointer-events-none">3.9 MB</p>
                        </li>
                        <li class="relative">
                          <div class="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
                            <img src="https://media.architecturaldigest.com/photos/55e77636302ba71f301700fb/master/w_900,h_600,c_limit/dam-images-daily-2014-09-gotham-sets-gotham-set-design-02-wayne-manor-library.jpg" alt="" class="object-cover pointer-events-none group-hover:opacity-75">
                            <button type="button" class="absolute inset-0 focus:outline-none">
                              <span class="sr-only">View details for IMG_4985.HEIC</span>
                            </button>
                          </div>
                          <p class="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">IMG_4985.HEIC</p>
                          <p class="block text-sm font-medium text-gray-500 pointer-events-none">3.9 MB</p>
                        </li>
                        <li class="relative">
                          <div class="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
                            <img src="https://gothamtrending.files.wordpress.com/2011/09/wayne-manor-entrance-interiors-osterly-park-the-dark-knight-rises.jpg?w=584" alt="" class="object-cover pointer-events-none group-hover:opacity-75">
                            <button type="button" class="absolute inset-0 focus:outline-none">
                              <span class="sr-only">View details for IMG_4985.HEIC</span>
                            </button>
                          </div>
                          <p class="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">IMG_4985.HEIC</p>
                          <p class="block text-sm font-medium text-gray-500 pointer-events-none">3.9 MB</p>
                        </li>
                        <li class="relative">
                          <div class="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
                            <img src="https://www.movie-locations.com/movies/b/Batman-1989-Library-Hatfield-House.jpg" alt="" class="object-cover pointer-events-none group-hover:opacity-75">
                            <button type="button" class="absolute inset-0 focus:outline-none">
                              <span class="sr-only">View details for IMG_4985.HEIC</span>
                            </button>
                          </div>
                          <p class="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">IMG_4985.HEIC</p>
                          <p class="block text-sm font-medium text-gray-500 pointer-events-none">3.9 MB</p>
                        </li>
                        <li class="relative">
                          <div class="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
                            <img src="https://i.pinimg.com/originals/96/d3/6a/96d36a2909f43552c1aad75d411712a6.jpg" alt="" class="object-cover pointer-events-none group-hover:opacity-75">
                            <button type="button" class="absolute inset-0 focus:outline-none">
                              <span class="sr-only">View details for IMG_4985.HEIC</span>
                            </button>
                          </div>
                          <p class="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">IMG_4985.HEIC</p>
                          <p class="block text-sm font-medium text-gray-500 pointer-events-none">3.9 MB</p>
                        </li>
                        <li class="relative">
                          <div class="group block w-full aspect-w-10 aspect-h-7 rounded-lg bg-gray-100 focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-offset-gray-100 focus-within:ring-indigo-500 overflow-hidden">
                            <img src="https://pbs.twimg.com/media/DonAdq5XsAERh_8.jpg" alt="" class="object-cover pointer-events-none group-hover:opacity-75">
                            <button type="button" class="absolute inset-0 focus:outline-none">
                              <span class="sr-only">View details for IMG_4985.HEIC</span>
                            </button>
                          </div>
                          <p class="mt-2 block text-sm font-medium text-gray-900 truncate pointer-events-none">IMG_4985.HEIC</p>
                          <p class="block text-sm font-medium text-gray-500 pointer-events-none">3.9 MB</p>
                        </li>

                        <!-- More files... -->
                      </ul>
                    </div>
                                        
               
                

                </x-slot>
            </div>
          </div>
        </main>
    </div>
</x-app-layout>