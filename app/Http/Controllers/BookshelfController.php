<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class BookshelfController extends Controller
{
    public function index(){

        
        return view('bookshelf/index');
        
    }

    public function book(){

        
        return view('bookshelf/book');
        
    }
}
