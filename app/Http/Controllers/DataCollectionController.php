<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DataCollectionController extends Controller
{
    

	public function beneficiaries_show(){

		$module = "secure-enclave";

	    return view('data-collection/beneficiaries/show');
	    
	}

	public function business_information_show(){

	    return view('data-collection/business-information/show');
	    
	}

	public function digital_world_show(){

	    return view('data-collection/digital-world/show');
	    
	}

	public function finance_show(){

	    return view('data-collection/finance/show');
	    
	}

	public function financial_advisors_show(){

	    return view('data-collection/financial-advisors/show');
	    
	}
	public function health_information_show(){

	    return view('data-collection/health-information/show');
	    
	}
	public function house_show(){

	    return view('data-collection/house/show');
	    
	}
	public function information_loved_ones_show(){

	    return view('data-collection/information-loved-ones/show');
	    
	}

	public function information_loved_ones_photos(){

	    return view('data-collection/information-loved-ones/photos');
	    
	}
	public function information_loved_ones_memoirs(){

	    return view('data-collection/information-loved-ones/memoirs');
	    
	}
	public function information_loved_ones_recordings(){

	    return view('data-collection/information-loved-ones/recordings');
	    
	}
	public function information_loved_ones_history(){

	    return view('data-collection/information-loved-ones/history');
	    
	}

	public function lifestyle_documents_show(){

	    return view('data-collection/lifecycle-documents/show');
	    
	}

	public function personal_information_show(){

	    return view('data-collection/personal-information/show');
	    
	}

	public function physical_assets_show(){

	    return view('data-collection/physical-assets/show');
	    
	}

	public function physical_property_show(){

	    return view('data-collection/physical-assets/property-show');
	    
	}

	public function physical_property_photos(){

	    return view('data-collection/physical-assets/property-photos');
	    
	}

	public function physical_property_notes(){

	    return view('data-collection/physical-assets/property-notes');
	    
	}

	public function requests_show(){

	    return view('data-collection/requests/show');
	    
	}

	public function subscriptions_memberships_show(){

	    return view('data-collection/subscriptions-memberships/show');
	    
	}

	public function will_executors_show(){

	    return view('data-collection/will-executors/show');
	    
	}
}
