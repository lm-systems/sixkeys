<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class MemorialController extends Controller
{
    public function home(){

        
        return view('memorial/home');
        
    }

    public function photos(){

        
        return view('memorial/photos');
        
    }

    public function journal(){

        
        return view('memorial/journal');
        
    }
}
