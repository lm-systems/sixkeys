<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class OnboardingController extends Controller
{
    public function onboarding_start(){

	    return view('onboarding/start');
	    
	}

	public function personal(){

	    return view('onboarding/personal');
	    
	}

	public function beneficiaries(){

	    return view('onboarding/beneficiaries');
	    
	}
	public function finances(){

	    return view('onboarding/finances');
	    
	}
	public function will(){

	    return view('onboarding/will');
	    
	}
	public function physical(){

	    return view('onboarding/physical');
	    
	}

	public function physical_property_show(){

	    return view('onboarding/physical_property_show');
	    
	}

	public function physical_property_photos(){

	    return view('onboarding/physical_property_photos');
	    
	}

	public function physical_property_notes(){

	    return view('onboarding/physical_property_notes');
	    
	}
	public function guardians(){

	    return view('onboarding/guardians');
	    
	}

	public function guardians_add(){

	    return view('onboarding/guardians_add');
	    
	}

	public function guardians_new(){

	    return view('onboarding/guardians_new');
	    
	}
	public function finish(){

	    return view('onboarding/finish');
	    
	}
}
