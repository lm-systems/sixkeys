<?php

namespace App\Http\Livewire;

use Livewire\Component;

class CategoryPanel extends Component
{

	public $panel_title;
	public $percentage;
	public $completed;
	public $path;
	public $link;

    public function render()
    {

    	

        return view('livewire.category-panel');
    }
}
