<?php

namespace App\Http\Livewire;

use Livewire\Component;

class MemorialNavigationBar extends Component
{
    public function render()
    {
        return view('livewire.memorial-navigation-bar');
    }
}
