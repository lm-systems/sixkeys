const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    purge: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],

    theme: {
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
            },
            colors: {
              Mongoose: {
                DEFAULT: "#BAAD88",
              },
              Sisal: {
                DEFAULT: "#DDD7C5",
              },
              DevPlat800: {
                DEFAULT: "#243B53",
              },
              DevPlat700: {
                DEFAULT: "#334E68",
              },
              DevPlat600: {
                DEFAULT: "#486581",
              }
            }
        },
    },

    variants: {
        extend: {
            cursor: ['disabled'],
            pointerEvents: ['disabled'],
            backgroundColor: ['disabled'],
        },
    },

    plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],
};
