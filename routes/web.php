<?php

use Illuminate\Support\Facades\Route;

use Auth0\Login\Auth0Controller;
use App\Http\Controllers\Auth\Auth0IndexController;

use App\Http\Controllers\DataCollectionController;
use App\Http\Controllers\OnboardingController;
use App\Http\Controllers\PlatformController;
use App\Http\Controllers\MemorialController;
use App\Http\Controllers\BookshelfController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/auth0/callback', [Auth0Controller::class, 'callback'])->name('auth0-callback');

Route::get('/login', [Auth0IndexController::class, 'login'])->name('login');
Route::get('/logout', [Auth0IndexController::class, 'logout'])->name('logout');
Route::get('/profile', [Auth0IndexController::class, 'profile'])->name('profile');

Route::middleware(['auth:sanctum', 'verified'])->group(function() {

    Route::get('/', [PlatformController::class, 'index']);

    Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard', function () {
        return view('dashboard');
    })->name('dashboard');


    Route::get('/beneficiaries', [DataCollectionController::class, 'beneficiaries_show']);
    Route::get('/business-information', [DataCollectionController::class, 'business_information_show']);
    Route::get('/digital-world', [DataCollectionController::class, 'digital_world_show']);
    Route::get('/finances', [DataCollectionController::class, 'finance_show']);
    Route::get('/financial-advisors', [DataCollectionController::class, 'financial_advisors_show']);
    Route::get('/health-information', [DataCollectionController::class, 'health_information_show']);
    Route::get('/house', [DataCollectionController::class, 'house_show']);
    Route::get('/information-loved-ones', [DataCollectionController::class, 'information_loved_ones_show']);
    Route::get('/information-loved-ones/photos', [DataCollectionController::class, 'information_loved_ones_photos']);
    Route::get('/information-loved-ones/memoirs', [DataCollectionController::class, 'information_loved_ones_memoirs']);
    Route::get('/information-loved-ones/recordings', [DataCollectionController::class, 'information_loved_ones_recordings']);
    Route::get('/information-loved-ones/family-history', [DataCollectionController::class, 'information_loved_ones_history']);

    Route::get('/lifestyle-documents', [DataCollectionController::class, 'lifestyle_documents_show']);
    Route::get('/personal-information', [DataCollectionController::class, 'personal_information_show']);
    Route::get('/physical-assets', [DataCollectionController::class, 'physical_assets_show']);
    Route::get('/physical-assets/property/property_show', [DataCollectionController::class, 'physical_property_show']);
    Route::get('/physical-assets/property/property_photos', [DataCollectionController::class, 'physical_property_photos']);
    Route::get('/physical-assets/property/property_notes', [DataCollectionController::class, 'physical_property_notes']);
    Route::get('/requests', [DataCollectionController::class, 'requests_show']);
    Route::get('/subscriptions-memberships', [DataCollectionController::class, 'subscriptions_memberships_show']);
    Route::get('/will-executors', [DataCollectionController::class, 'will_executors_show']);

    Route::get('/onboarding/start', [OnboardingController::class, 'onboarding_start']);
    Route::get('/onboarding/personal', [OnboardingController::class, 'personal']);
    Route::get('/onboarding/beneficiaries', [OnboardingController::class, 'beneficiaries']);
    Route::get('/onboarding/finances', [OnboardingController::class, 'finances']);
    Route::get('/onboarding/will', [OnboardingController::class, 'will']);
    Route::get('/onboarding/physical', [OnboardingController::class, 'physical']);
    Route::get('/onboarding/physical_property_show', [OnboardingController::class, 'physical_property_show']);
    Route::get('/onboarding/physical_property_photos', [OnboardingController::class, 'physical_property_photos']);
    Route::get('/onboarding/physical_property_notes', [OnboardingController::class, 'physical_property_notes']);
    Route::get('/onboarding/guardians', [OnboardingController::class, 'guardians']);
    Route::get('/onboarding/guardians_add', [OnboardingController::class, 'guardians_add']);
    Route::get('/onboarding/guardians_new', [OnboardingController::class, 'guardians_new']);
    Route::get('/onboarding/finish', [OnboardingController::class, 'finish']);

    //Memorial Routes
    Route::get('/memorial/home', [MemorialController::class, 'home']);
    Route::get('/memorial/photos', [MemorialController::class, 'photos']);
    Route::get('/memorial/journal', [MemorialController::class, 'journal']);

    //Bookshelf Routes
    Route::get('/bookshelf', [BookshelfController::class, 'index']);
    Route::get('/bookshelf/book', [BookshelfController::class, 'book']);
});



